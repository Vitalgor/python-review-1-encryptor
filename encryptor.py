import argparse
import pickle
import string
import sys
import Config
from collections import Counter


class ParseError(Exception):
    pass


def read_input(input_file_name):
    if input_file_name:
        try:
            with open(input_file_name, 'r') as input_file:
                input_str = input_file.read()
        except Exception as e:
            raise ParseError("A problem has occurred till input reading: " + str(e))
    else:
        input_str = sys.stdin.read()
    return input_str


def print_output(output_file_name, output):
    if output_file_name:
        with open(output_file_name, 'w') as output_file:
            output_file.write(output)
    else:
        print(output)


def n_grams_metrics(curr_decode, frequencies, n_grams_len):
    score = 0
    for i in range(n_grams_len, len(curr_decode)):
        sequence = curr_decode[i - n_grams_len:i]
        letter = curr_decode[i]
        score += frequencies.get(sequence, dict()).get(letter, 0)
    return score


def get_frequencies(curr_decode):
    curr_frequencies = dict(Counter(curr_decode))
    for symbol in curr_frequencies:
        curr_frequencies[symbol] /= len(curr_decode)
    return curr_frequencies


def frequency_metric(frequencies, shift, the_universe_alphabet, curr_frequencies):
    score = 0
    for current_symbol in curr_frequencies:
        length = len(the_universe_alphabet[0])
        symbol_pos = (the_universe_alphabet[1].get(current_symbol) - shift + length) % length
        symbol = the_universe_alphabet[0][symbol_pos]
        score += (frequencies.get(symbol, 0) - curr_frequencies[current_symbol]) ** 2
    return score


def calculate_index(strings, the_universe_alphabet):
    frequencies = list()
    score = 0
    for i, current_string in enumerate(strings):
        cnt = Counter(current_string)
        frequencies.append(dict())
        for c in the_universe_alphabet[0]:
            frequencies[i][c] = cnt[c] / len(current_string)
        if i > 0:
            for k in range(i):
                for c in the_universe_alphabet[0]:
                    score += frequencies[i].get(c, 0) * frequencies[k].get(c, 0)
    return 2 * score / (len(strings) * (len(strings) - 1))



def decode_caesar(str_to_decode, key, the_universe_alphabet):
    decoded_str = []
    for c in str_to_decode:
        index = the_universe_alphabet[1].get(c, -1)
        if index == -1:
            raise ParseError("Unsupported symbol: " + c)
        index += len(the_universe_alphabet[0]) - key
        index %= len(the_universe_alphabet[0])
        decoded_str.append(the_universe_alphabet[0][index])
    return ''.join(decoded_str)


def hack_caesar(str_to_decode, data, the_universe_alphabet):
    best_score = -1
    best_shift = 0
    curr_decode = decode_caesar(str_to_decode, 0, the_universe_alphabet)
    best_decode = None
    curr_frequencies = get_frequencies(curr_decode)
    for key in range(len(the_universe_alphabet[0])):
        if data['use_n_grams']:
            curr_decode = decode_caesar(str_to_decode, key, the_universe_alphabet)
            curr_score = n_grams_metrics(curr_decode, data['frequencies'], data['n-gram-len'])
            if best_score == -1 or curr_score > best_score:
                best_score = curr_score
                best_decode = curr_decode
        else:
            curr_score = frequency_metric(data['frequencies'], key, the_universe_alphabet, curr_frequencies)
            if best_score == -1 or curr_score < best_score:
                best_score = curr_score
                best_shift = key

    if data['use_n_grams']:
        return best_decode
    return decode_caesar(str_to_decode, best_shift, the_universe_alphabet)


def parse_args():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='mode')

    parser_encode = subparsers.add_parser('encode')
    parser_encode.add_argument('--cipher', choices=['caesar', 'vigenere'], required=True, help='Using cipher')
    parser_encode.add_argument('--key', required=True, help='Using key for cipher')
    parser_encode.add_argument('--input-file', help='file for text input')
    parser_encode.add_argument('--output-file', help='file for text output')
    parser_encode.set_defaults(func=encode)

    parser_decode = subparsers.add_parser('decode')
    parser_decode.add_argument('--cipher', choices=['caesar', 'vigenere'], required=True, help='Using cipher')
    parser_decode.add_argument('--key', required=True, help='Using key for cipher')
    parser_decode.add_argument('--input-file', help='file for text input')
    parser_decode.add_argument('--output-file', help='file for text output')
    parser_decode.set_defaults(func=decode)

    parser_train = subparsers.add_parser('train')
    parser_train.add_argument('--text-file', help='file for text input')
    parser_train.add_argument('--model-file', required=True, help='file for model')
    parser_train.add_argument('--n-grams-len', type=int, help='length for n-grams(0 if they are not used)')
    parser_train.set_defaults(func=train)

    parser_hack = subparsers.add_parser('hack')
    parser_hack.add_argument('--input-file', help='file for text input')
    parser_hack.add_argument('--output-file', help='file for text output')
    parser_hack.add_argument('--model-file', required=True, help='file with model')
    parser_hack.add_argument('--cipher', choices=['caesar', 'vigenere'], default='caesar',
                             help='Optional: using cipher.Default caesar')
    parser_hack.set_defaults(func=hack)

    return parser.parse_args()


def encode_caesar(args, str_to_encode, the_universe_alphabet):
    encoded_str = []
    if not args.key.isdigit():
        raise ParseError("Invalid key")
    else:
        key = int(args.key)
        while key < 0:
            key += len(the_universe_alphabet[0])
        key %= len(the_universe_alphabet[0])

    for c in str_to_encode:
        index = the_universe_alphabet[1].get(c, -1)
        if index == -1:
            raise ParseError("Unsupported symbol: " + c)
        index += key
        index %= len(the_universe_alphabet[0])
        encoded_str.append(the_universe_alphabet[0][index])
    return encoded_str


def encode_vigenere(args, str_to_encode, the_universe_alphabet):
    encoded_str = []
    key = args.key
    while len(key) < len(str_to_encode):
        key += args.key
    key_index = 0
    for c in str_to_encode:
        current_key = the_universe_alphabet[1].get(key[key_index], -1)
        if current_key == -1:
            raise ParseError("Unsupported symbol: " + key[key_index])
        index = the_universe_alphabet[1].get(c, -1)
        if index == -1:
            raise ParseError("Unsupported symbol: " + c)
        index += current_key
        index %= len(the_universe_alphabet[0])
        encoded_str.append(the_universe_alphabet[0][index])
        key_index += 1
    return encoded_str


def encode(args, the_universe_alphabet):
    str_to_encode = read_input(args.input_file)
    encoded_str = []

    if args.cipher == 'caesar':
        encoded_str = encode_caesar(args, str_to_encode, the_universe_alphabet)

    if args.cipher == 'vigenere':
        encoded_str = encode_vigenere(args, str_to_encode, the_universe_alphabet)

    encoded_str = ''.join(encoded_str)
    print_output(args.output_file, encoded_str)


def decode_vigenere(args, str_to_decode, the_universe_alphabet):
    decoded_str = []
    key = args.key
    while len(key) < len(str_to_decode):
        key += args.key
    key_index = 0

    for c in str_to_decode:
        current_key = the_universe_alphabet[1].get(key[key_index], -1)
        if current_key == -1:
            raise ParseError("Unsupported symbol: " + key[key_index])
        index = the_universe_alphabet[1].get(c, -1)
        if index == -1:
            raise ParseError("Unsupported symbol: " + c)
        index += len(the_universe_alphabet[0]) - current_key
        index %= len(the_universe_alphabet[0])
        decoded_str.append(the_universe_alphabet[0][index])
        key_index += 1
    decoded_str = ''.join(decoded_str)
    return decoded_str


def decode(args, the_universe_alphabet):
    str_to_decode = read_input(args.input_file)
    decoded_str = []

    if args.cipher == 'caesar':
        if not args.key.isdigit():
            raise ParseError("Invalid key")
        else:
            key = int(args.key)
            while key < 0:
                key += len(the_universe_alphabet[0])
            key %= len(the_universe_alphabet[0])

        decoded_str = decode_caesar(str_to_decode, key, the_universe_alphabet)

    if args.cipher == 'vigenere':
        decoded_str = decode_vigenere(args, str_to_decode, the_universe_alphabet)

    print_output(args.output_file, decoded_str)


def train(args, the_universe_alphabet):
    str_to_train = read_input(args.text_file)

    if not args.n_grams_len:
        # Simple frequency analysis
        frequencies = dict(Counter(str_to_train))
        for symbol in frequencies:
            frequencies[symbol] /= len(str_to_train)
        data = {'use_n_grams': False, 'frequencies': frequencies}
    else:
        if args.n_grams_len < 2:
            raise ParseError("Invalid n-grams count value ", args.n_grams_len)
        n_grams_counts = dict()  # count of a letter after an (n-1) sequence
        for i in range(args.n_grams_len, len(str_to_train)):
            sequence = str_to_train[i - args.n_grams_len:i]
            if sequence not in n_grams_counts.keys():
                n_grams_counts[sequence] = dict()
            letter = str_to_train[i]
            if letter not in n_grams_counts[sequence].keys():
                n_grams_counts[sequence][letter] = 0
            n_grams_counts[sequence][letter] += 1
        # Transform dict of counts into frequencies
        # Here be dragons
        n_grams_frequencies = {sequence: {letter: count / sum(counts.values()) for (letter, count) in counts.items()}
                               for (sequence, counts) in n_grams_counts.items()}
        data = {'use_n_grams': True, 'n-gram-len': args.n_grams_len, 'frequencies': n_grams_frequencies}

    try:
        with open(args.model_file, 'wb') as model_file:
            pickle.dump(data, model_file)
    except Exception as e:
        raise ParseError("A problem has occurred till model saving: " + str(e))


def hack_vigenere(str_to_decode, data, the_universe_alphabet):
    best_decode = []
    best_index = float("inf")
    best_strings_split = None
    for key_len in range(2, Config.max_hack_key):
        strings = list()
        for i in range(key_len):
            strings.append(str_to_decode[i::key_len])
        index = calculate_index(strings, the_universe_alphabet)
        if index < best_index:
            best_index = index
            best_strings_split = strings

    decoded_parts = []
    max_len = 0
    for s in best_strings_split:
        max_len = max(max_len, len(s))
        decoded_parts.append(hack_caesar(s, data, the_universe_alphabet))

    for i in range(max_len):
        for s in decoded_parts:
            if i < len(s):
                best_decode.append(s[i])
    best_decode = ''.join(best_decode)
    return best_decode


def hack(args, the_universe_alphabet):
    str_to_decode = read_input(args.input_file)
    try:
        with open(args.model_file, 'rb') as model_file:
            data = pickle.load(model_file)
    except Exception as e:
        raise ParseError("A problem has occurred till model reading: " + str(e))

    best_decode = []

    if args.cipher == 'caesar':
        best_decode = hack_caesar(str_to_decode, data, the_universe_alphabet)

    if args.cipher == 'vigenere':
        best_decode = hack_vigenere(str_to_decode, data, the_universe_alphabet)

    print_output(args.output_file, best_decode)


def prepare_alphabet():
    the_universe_alphabet = [None, None]
    the_universe_alphabet[0] = string.printable + 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя' + 'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ'
    the_universe_alphabet[1] = dict()
    i = 0
    for c in the_universe_alphabet[0]:
        the_universe_alphabet[1][c] = i
        i += 1

    return the_universe_alphabet


def main():
    args = parse_args()
    the_universe_alphabet = prepare_alphabet()
    args.func(args, the_universe_alphabet)


if __name__ == "__main__":
    main()
